#!/bin/bash
set -xe

#_BUILD_TOOLS_REPO_URL=https://gitlab.com/ubports/community-ports/halium-generic-adaptation-build-tools
#_BUILD_TOOLS_BRANCH=halium-10-focal
_BUILD_TOOLS_REPO_URL=https://gitlab.com/amartinz/halium-generic-adaptation-build-tools.git
_BUILD_TOOLS_BRANCH=main

[ -d build ] || git clone $_BUILD_TOOLS_REPO_URL -b $_BUILD_TOOLS_BRANCH build
./build/build.sh "$@"
